terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "DO_TOKEN" {}

provider "digitalocean" {
  token = var.DO_TOKEN
}
