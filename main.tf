data "digitalocean_ssh_key" "m3_key" {
  name = "MacBook-Pro-M3-Max"
}

resource "digitalocean_droplet" "vpnserver" {
  image = "ubuntu-23-10-x64"
  name = "vpn-server"
  region = "ams3"
  size = "s-1vcpu-1gb"
  user_data = file("setup.sh")
  ssh_keys = [
    data.digitalocean_ssh_key.m3_key.id
  ]
}
