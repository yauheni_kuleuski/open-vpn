
init:
	@. .env
	terraform init

plan:
	@. .env
	terraform plan

apply:
	@. .env
	terraform apply -auto-approve

ssh:
	@. .env
	@$(eval droplet_ip := $(shell terraform output droplet_ip))
	ssh root@$(droplet_ip)

download_cfg:
	@. .env
	@rm ~/Documents/client.ovpn
	@$(eval droplet_ip := $(shell terraform output droplet_ip))
	@scp root@$(droplet_ip):/root/client.ovpn ~/Documents/client.ovpn
	@ls -la ~/Documents/client.ovpn

destroy:
	@. .env
	terraform destroy -auto-approve
